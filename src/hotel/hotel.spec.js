// @flow

import HotelManagement from './hotelManagement';
import {DomainEventDispatcher, testLogger} from 'server-commons';
import {HotelTestRepository} from './hotelRepository';

const dispatcher = new DomainEventDispatcher();

describe('HotelManagement:', () => {
  let hotelManagement: HotelManagement;

  beforeEach(() => {

    hotelManagement = new HotelManagement(new HotelTestRepository(), dispatcher, testLogger);
  });

  describe('when no one has allowed invitations', () => {
    it('should return empty list', async () => {
      expect(await hotelManagement.readAllParticipants()).toHaveLength(0);
    });
  });
});
