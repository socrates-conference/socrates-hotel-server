//@flow

import type {Participant} from './hotelManagement';
import MySql from '../db/MySql';

export interface HotelRepository {
  readAll(): Promise<Array<Participant>>;
}

export default class HotelMySqlRepository implements HotelRepository {
  _mysql: MySql;

  constructor(mysql: MySql) {
    this._mysql = mysql;
  }

  async readAll(): Promise<Array<Participant>> {
    const queryStatement = 'SELECT * FROM participants';
    return await this._mysql.select(queryStatement);
  }
}

export class HotelTestRepository implements HotelRepository {
  _participants: Map<number, Participant>;
  _count: number;

  constructor() {
    this._participants = new Map();
    this._count = 0;
  }

  async readAll() {
    return [...this._participants.values()];
  }

}
