// @flow

import stream from 'stream';
import type {Logger} from 'server-commons/src/logger';
import type {SoCraTesEvent} from 'server-commons/src/streams/socratesEvent';
import HotelManagement from './hotelManagement';

export default class HotelMessageSink extends stream.Writable {
  _logger: Logger;
  _hotelManagement: HotelManagement;

  constructor(hotelManagement: HotelManagement, logger: Logger) {
    super({objectMode: true});
    this._logger = logger;
    this._hotelManagement = hotelManagement;
    this._initEventHandlers();
  }

  _initEventHandlers(): void {
    this.on('error', err => {
      this._logger.error('Error while using HotelMessageSink stream', err);
    });

    this.on('close', () => {
      this._logger.info('HotelMessageSink stream was closed.');
    });

    this.on('finish', () => {
      this._logger.info('HotelMessageSink stream finished.');
    });
  }

  //noinspection JSUnusedGlobalSymbols,JSUnusedLocalSymbols
  // $FlowFixMe Type definition is broken: return type should be void, not boolean
  async _write(chunk: any, encoding: string, callback: (err?: Error, data?: any) => void): void {
    await this._receiveMessage(chunk);
    callback();
  }

  async _receiveMessage(event: SoCraTesEvent): Promise<void> {
    //ToDo whatever
    if (event.event === 'ALLOW_INVITATIONS') {
      this._hotelManagement.allow(event.value);
    } else if (event.event === 'DENY_INVITATIONS') {
      this._hotelManagement.deny(event.value);
    }
  }
}
