// @flow

import express from 'express';
import type {Middleware} from 'server-commons/src/middleware';
import type {EventDispatcher} from 'server-commons/src/domain/eventDispatcher';
import HotelManagement from '../hotelManagement';
import HotelApiVersion1 from './hotelApiVersion1';

export default function createHotelServer(invitables: HotelManagement, dispatcher: EventDispatcher): Middleware {
  return (app: express.Application) => app.use('/api/v1/',
    new HotelApiVersion1(invitables, dispatcher));
}
