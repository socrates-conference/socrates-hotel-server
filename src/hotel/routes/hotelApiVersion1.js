// @flow

import * as express from 'express';
import HotelManagement from '../hotelManagement';
import type {EventDispatcher} from 'server-commons/src/domain/eventDispatcher';

export default class HotelApiVersion1 extends express.Router {
  _hotelManagement: HotelManagement;
  _dispatcher: EventDispatcher;

  constructor(invitables: HotelManagement, dispatcher: EventDispatcher) {
    super();
    this._hotelManagement = invitables;
    this._dispatcher = dispatcher;
    this._initRoutes();
  }

  _initRoutes = () => {

    this.get('/hotel/participants', async (req: express.Request, res: express.Response) => {
      const participants = await this._hotelManagement.readAllParticipants();
      res.status(200).send(participants).end();
    });
  };
}


