// @flow

import type {EventDispatcher} from 'server-commons';
import {Aggregate} from 'server-commons';
import type {Logger} from 'server-commons/src/logger';
import {HotelRepository} from './hotelRepository';

export type Participant = {
  id: ?number,
  personId: number,
  firstname: string,
  lastname: string,
  email?: string,
  nickname?: string,
  company?: string,
  arrival: string,
  departure: string,
  address1: string,
  address2?: string,
  postal: string,
  city: string,
  country: string,
  confirmed?: boolean,
  dietary: boolean,
  dietaryInfo?: string,
  family: boolean,
  familyInfo?: string,
}

export default class HotelManagement extends Aggregate {
  _logger: Logger;
  _repository: HotelRepository;

  constructor(repository: HotelRepository, dispatcher: EventDispatcher, logger: Logger) {
    super(dispatcher);
    this._repository = repository;
    this._logger = logger;
  }

  async readAllParticipants(): Promise<Array<Participant>> {
    return await this._repository.readAll();
  }

}
