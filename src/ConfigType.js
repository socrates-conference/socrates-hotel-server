// @flow

import type {Config} from 'server-commons/src/ConfigType';

export type HotelManagementConfig = Config & {
  app: {
    name: string,
    logLevel: string
  },
};

